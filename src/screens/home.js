import { Button, Col, Row, Progress } from 'antd';
import React, { Fragment, useEffect,useState } from 'react';
import CardIcon from '../components/cardIcon.jsx';
import { BiDownArrowAlt, BiUpArrowAlt } from 'react-icons/bi';
import IconCardWithPercent from '../components/IconCardWithPercent.jsx';

import WalletSection from '../components/walletSection.jsx';
import Transaction from '../components/transaction.jsx';
import { getCurrentAccountBalance,getPrice } from "../services/api";
import BitCoinicon from '../images/bitcoin.png';
import Stellaricon from '../images/coin.png';
import Ethericon from '../images/ether.png';

const Home = () => {

    const [coinsPrices,setCoinsPrices]=useState({
        bitcoins:{},
        ether:{},
        stellar:{}
    })
    const [loading,setLoading]=useState(false)
    useEffect(() => {
        onFetchBlockChangeApi()
    }, [])
    const onFetchBlockChangeApi = async () => {
        try {
            setLoading(true)
           let currentBTC= await getPrice("BTC-USD")
           let currentETH= await getPrice("ETH-USD")
           let currentXLM= await getPrice("XLM-USD")
           setCoinsPrices({
               bitcoins:currentBTC.data,
               ether:currentETH.data,
               stellar:currentXLM.data
           })
           setLoading(false)
        } catch (err) {
            setLoading(false)
            alert(err.message)
        }
    }

    return (
        <Fragment>
            <div className='page'>
                <section className='bg-white container card-page'>
                    <Row>
                        <Col md={8} xs={24} className='border-end'>
                            <div>
                                <div className='mb-5'>
                                    <h6 className='fw-bold text-secondary'>BALANCE DETAILS</h6>
                                    <div className='price'>$ 2,128,002.00</div>
                                    <div className='sub-txt'>3.302343423324 &nbsp; BTC</div>
                                </div>
                                <Row>
                                    <Col span={12}>
                                        <CardIcon Icon={BiDownArrowAlt} price="3,2345.00" bg="#dff7ff" text="INCOME" />
                                    </Col>
                                    <Col span={12}>
                                        <CardIcon Icon={BiUpArrowAlt} price="1,45.00" bg="#dae1fc" text="Expense" />
                                    </Col>
                                </Row>

                                <Row className='mt-4'>
                                    <Col span={7}>
                                        <Button block className="btn-sky">RECEIVE</Button>

                                    </Col>
                                    <Col span={7} offset={5}>
                                        <Button block className="btn-prpl ">SEND</Button>

                                    </Col>
                                </Row>
                            </div>
                        </Col>
                        <Col md={16} xs={24}>

                            <Row className='my-3'>
                                <Col md={14} xs={24} className='text-center'>
                                    <Progress
                                        width={200}
                                        type="circle"
                                        strokeColor={{
                                            '10%': 'orange',
                                            '20%': '#8b98a1',
                                            '100%': 'blue',
                                        }}
                                        percent={76}
                                        format={percent => <>{percent} %<br /><span style={{ fontSize: "16px" }}>BTC</span></>}
                                    />
                                </Col>
                                <Col md={8} xs={24} className='my-3'>
                                    <IconCardWithPercent
                                        price="BTC"
                                        bg="#fff2e3"
                                        text="Bitcoin"
                                        Icon={() => (<img src={BitCoinicon} className='walletIcons' />)}
                                        percent="12%"
                                        hideDollar={true}
                                    />
                                    <IconCardWithPercent
                                        price="ETH"
                                        bg="#e8ebfe"
                                        text="Ether"
                                        Icon={() => (<img src={Ethericon} className='walletIcons' />)}
                                        percent="45%"
                                        hideDollar={true}

                                    />
                                    <IconCardWithPercent
                                        price="XLM"
                                        bg="#dcf5fd"
                                        text="Stellar"
                                        percent="76%"
                                        hideDollar={true}
                                        Icon={() => (<img src={Stellaricon} className='walletIcons' />)}
                                    />

                                </Col>
                            </Row>
                        </Col>

                    </Row>
                </section>
                <section className='container my-5'>
                    <WalletSection coinsPrices={coinsPrices}/>
                </section>
                <section className='container my-5'>
                    <Transaction />
                </section>
            </div>
        </Fragment>
    );
}

export default Home;