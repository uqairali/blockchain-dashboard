import resolve from "./resolve";
import axios from 'axios'
let apiBase = process.env.REACT_APP_API_BASEURL;




//get current account balance
export const getCurrentAccountBalance = async () => {
    return await resolve(axios.get(apiBase + "accounts/primary/BTC").then(res => res.data))
}
//get current price
export const getPrice = async (symbol) => {
    return await resolve(axios.get(apiBase + `tickers/${symbol}`).then(res => res.data))
}