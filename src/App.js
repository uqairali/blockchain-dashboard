import './App.less';
import React from "react";
import axios from 'axios';
import MainRouter from './router/appRouter';
//set axios default header
axios.defaults.headers.common['X-API-Token'] = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkFQSSJ9.eyJhdWQiOiJtZXJjdXJ5IiwidWlkIjoiYTBkYzUxMGYtNjc0OS00NDUzLTk1NDUtMjBiYzdhMTRkNTE3IiwiaXNzIjoiYmxvY2tjaGFpbiIsInJkbyI6ZmFsc2UsImlhdCI6MTY0MjE2MTQzNCwianRpIjoiNTY5ZGJhYjAtNmFhMi00NzFhLTlhNWEtYjU4OWY0NGFkYWIzIiwic2VxIjo1MDEzNDY3LCJ3ZGwiOnRydWV9.IFAM92wXI4+UI13sjBofJV4w2TI8eVrDj+394fvKgqThawn4882efGsuy/6uyuTAVdXhQF9RF6+B6MFOBGwdfkY=";
axios.defaults.headers.common['acceipt']="application/json";
const App = () => {
  return (
    <MainRouter />
  );
}

export default App;
