import React from "react";
import { Router, Route, Switch } from "react-router-dom";
import MainTemplate from "./../templates/mainTemplate";
import Home from "./../screens/home";
import { createBrowserHistory } from 'history'

const history = createBrowserHistory();


const MainRouter = () => {
  return (
    <Router history={history}>
      <Switch>  
        <MainTemplate exact path="/" component={Home} />     
        <Route path="*" component={Home} />
      </Switch>
    </Router>
  );
};

export default MainRouter;
