import React from 'react'

const WalletCard = ({ Icon, bgcard, text ,currentPrice,type}) => {
    return (
        <div style={{
            height: 270,
            background: bgcard,
            borderRadius: 25,
            display: 'flex',
            flexDirection: 'column',

        }}>
            <div className='p-4 flex-grow-1' >
                <div className='icon-box'
                    style={{ background: '#fff', width: 40 }}>
                    <Icon size={25} />
                </div>
                <h6 className='text-white mt-3'>{text}</h6>
            </div>
            <div className='px-4 '>
                <h5 className='fw-bold text-white'>${currentPrice?.price_24h}</h5>
                <p className='text-secondary fw-bold'>{currentPrice?.volume_24h?.toFixed(2)} {type}</p>
            </div>
        </div>
    )
}

export default WalletCard
