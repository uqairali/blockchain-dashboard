import { Col, Row } from 'antd'
import React, { Fragment } from 'react'
import WalletCard from './walletCard'
import TradingFee from './tradingFee';
import BitCoinicon from '../images/bitcoin.png';
import Stellaricon from '../images/coin.png';
import Ethericon from '../images/ether.png';
import Refresh1Icon from '../images/refresh1.png';
import Refresh2Icon from '../images/refresh2.png';
const WalletSection = ({coinsPrices}) => {
    return (
        <Fragment>
            <Row justify='center'>
                <Col md={17} xs={24} className='my-3'>
                    <Row justify='space-between' style={{ paddingRight:"5rem" }}>
                        <Col>
                            <h6 className='text-secondary fw-bold'>WALLETS</h6>
                        </Col>
                        <Col>
                            <h6 className='text-secondary '>More ></h6>
                        </Col>
                    </Row>
                    <Row gutter={[20, 20]} justify='left'>
                        <Col md={5} xs={12}>
                            <WalletCard
                                bgcard={"#f8b61d"}
                                text="Bitcoin"
                                type="BTC"
                                currentPrice={coinsPrices.bitcoins}
                                Icon={() => (<img src={BitCoinicon} className='walletIcons' />)}
                            />
                        </Col>
                        <Col md={5} xs={12}>
                            <WalletCard
                                bgcard={"#4cacfd"}
                                text="Ether"
                                type="ETH"
                                currentPrice={coinsPrices.ether}
                                Icon={() => (<img src={Ethericon} className='walletIcons' />)}
                            />
                        </Col>
                        <Col md={5} xs={12}>
                            <WalletCard
                                bgcard={"#4bd7f9"}
                                text="Stellar"
                                type="XLM"
                                currentPrice={coinsPrices.stellar}
                                Icon={() => (<img src={Stellaricon} className='walletIcons' />)}
                            />
                        </Col>

                    </Row>

                </Col>
                <Col md={7} xs={24} className='my-3'>

                    <Row >
                        <Col span={16} xs={24}>
                            <Row justify='space-between'>
                                <Col>
                                    <h6 className='text-secondary fw-bold'>
                                        TRADING FEE
                                    </h6>
                                </Col>
                                <Col>
                                    <h6 className='text-secondary '>More ></h6>
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                    <Row className="mb-3">
                        <Col md={16} xs={24}>
                            <TradingFee
                                price="0.069%"
                                bg="#dff7ff"
                                text="Maker" 
                                icon={Refresh2Icon}
                                />
                        </Col>
                    </Row>
                    <Row>
                        <Col md={16} xs={24}>
                            <TradingFee
                                price="0.075%"
                                bg="#dae1fc"
                                text="Taker" 
                                icon={Refresh1Icon}
                                />
                        </Col>
                    </Row>
                </Col>
            </Row>


        </Fragment >
    )
}

export default WalletSection
