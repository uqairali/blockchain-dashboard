import { Col, Row, Switch } from 'antd'
import React, { Fragment, useState } from 'react'
import { BiUpArrowAlt, BiDownArrowAlt } from 'react-icons/bi'
import { FaAddressCard, FaPhone } from 'react-icons/fa'
import CardIcon from './cardIcon'

const Transaction = () => {
    const [secureIdentity, setSecureIdentity] = useState(false)
    const [securePhone, setSecurePhone] = useState(false)
    return (
        <Fragment>
            <Row justify='space-between' gutter={24}>
                <Col md={11} xs={24} className='my-3'>
                    <Row justify='space-between' className='mb-4' >
                        <Col>
                            <h6 className='text-secondary fw-bold'>Transaction</h6>
                        </Col>
                        <Col>
                            <h6 className='text-secondary '>More ></h6>
                        </Col>
                    </Row>

                    <Row justify='space-between' className='mb-4' >
                        <Col>
                            <CardIcon hideDollar={true} Icon={BiUpArrowAlt} price="Sent USDT" bg="#fff" text="23 Feb,2022" />
                        </Col>
                        <Col>
                            <div className='prple'>
                                -$1,232.00
                            </div>
                        </Col>
                    </Row>

                    <Row justify='space-between' >
                        <Col>
                            <CardIcon hideDollar={true} Icon={BiDownArrowAlt} price="Received USDT" bg="#fff" text="23 Feb,2022" />
                        </Col>
                        <Col>
                            <div className='prple'>
                                +$1,232.00
                            </div>
                        </Col>
                    </Row>
                </Col>
                <Col md={10} xs={24} className='my-3'>
                    <Row justify='space-between' gutter={12} className='mb-4'>
                        <Col >
                            <h6 className='text-secondary fw-bold'>Security</h6>
                        </Col>
                        <Col>
                            <h6 className='text-secondary '>More></h6>
                        </Col>
                    </Row>
                    <Row gutter={[20, 20]} justify='center'>
                        <Col md={12} xs={24}>
                            <div className='sec-card'>
                                <Row justify='space-between' >
                                    <Col>
                                        <CardIcon hideDollar={true} Icon={FaAddressCard} price="Identity" bg="#fff" text={secureIdentity ? "Enable" : "Disable"} />
                                    </Col>
                                    <Col>
                                        <Switch onChange={() => {
                                            setSecureIdentity(!secureIdentity)
                                        }} size="small" />
                                    </Col>
                                </Row>
                            </div>


                        </Col>
                        <Col md={12} xs={24}>
                            <div className='sec-card'>
                                <Row justify='space-between' >
                                    <Col>
                                        <CardIcon hideDollar={true} Icon={FaPhone} price="Phone" bg="#fff" text={securePhone ? "Enable" : "Disable"} />
                                    </Col>
                                    <Col>
                                        <Switch onChange={() => {
                                            setSecurePhone(!securePhone)
                                        }} size="small" />
                                    </Col>
                                </Row>
                            </div>
                        </Col>
                    </Row>
                </Col>
            </Row>
        </Fragment>
    )
}

export default Transaction
