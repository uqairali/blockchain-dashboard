import React from 'react';
import { Layout } from 'antd';
import { Route } from "react-router-dom";
import SiteHeader from './siteHeader';

const { Content } = Layout;


const MainTemplate = (props) => {

    const { exact, component, path } = props;
    console.log(props)
    return (
        <Layout >
            <SiteHeader />
            <Layout className="main-layout">
                <Content>
                    <Route exact={exact} component={component} path={path} />
                </Content>
            </Layout>
        </Layout>
    );
}


export default MainTemplate;