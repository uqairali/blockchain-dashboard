import React from 'react'
import { Layout, Menu } from 'antd';
import Userlogo from "../images/user.png";
import BillLogo from "../images/bell.png";
import StreamexLogo from "../images/streamexLogo.png";
import {
    SettingOutlined 
} from '@ant-design/icons';
const { Header } = Layout;

const SiteHeader = () => {
    return (
        <Header className="header">
        <Menu mode="horizontal" className="responsive-header">
        <Menu.Item key="1">
        <img src={StreamexLogo} className="streamex-Logo" alt="broken" />
        <b style={{marginLeft:"10px"}}>STREAMEX</b>

          </Menu.Item>
          <Menu.Item key="1">
                DASHBOARD
          </Menu.Item>
          <Menu.Item key="2">
                EXCHANGE
          </Menu.Item>
          <Menu.Item key="3">
                STE BANK
          </Menu.Item>
          <Menu.Item key="4">
                AFFILIATE
          </Menu.Item>
          <Menu.Item key="5">
                ABOUT
          </Menu.Item>
         
          <Menu.Item key="6">
              <SettingOutlined/>
          </Menu.Item>
          <Menu.Item key="7">
            <img src={BillLogo} className="bill-Logo" alt="broken" />
          </Menu.Item>
          <Menu.Item key="8">
            <img src={Userlogo} className="user-Logo" alt="broken" />
          </Menu.Item>
         
        </Menu>
       
      </Header>
    )
}

export default SiteHeader
